# Neighbourhood-assemblies

Policy on developing a formal layer of neighbourhood assemblies to be added to the bottom level of Aotearoa NZ democracy.

Implementation of open, inclusive, local grassroot community meetings as a method to enhance, deepen and increase participation in processes of local democracy and administration, and to strengthen reciprocity of communication between grassroots interests and broader administrative and governance structures.

See Loomio deliberation on this topic @ https://www.loomio.org/d/o2pwlGv9/community-development-at-the-roots-of-society-in-nz
